# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If running interactively, then:
if [ "$PS1" ]; then

    # don't put duplicate lines in the history. See bash(1) for more options
    export HISTCONTROL=ignoredups
    if [ -x ~/bin/sane_editor ]; then
	EDITOR=~/bin/sane_editor;
    elif [ -x /usr/bin/jed ]; then
	EDITOR="/usr/bin/jed";
    elif [ -x /usr/bin/vi ]; then
	EDITOR="/usr/bin/vi";
    fi;

    if [ -n "$EDITOR" ]; then
	export EDITOR;
    fi;
    # bts debian options

    if [ -e bin/faf ]; then
	complete -F _command faf
    fi;

    export DEBEMAIL="don@debian.org"
    export DEBHOME="$(eval 'echo' '~/projects/debian')"

    export GTK_IM_MODULE=xim
    export QT_IM_MODULE=xim

    # enable color support of ls and also add handy aliases
    eval `dircolors -b`
    alias ls='ls --color=auto'

    # See if we're in a chroot
    if [ -z "$debian_chroot" ] && [ -r /etc/debian_chroot ]; then
	debian_chroot=$(cat /etc/debian_chroot)
    fi

    # we want to set the color based on what machine we're on
    # if it's a remote, SSH_CONNECTION will be set
    # set variable identifying the chroot you work in (used in the prompt below)
    
    # disable terminal bell
    bind 'set bell-style none'

    if [ -n "$SSH_CONNECTION" -o -n "$SSH_CLIENT" ]; then
	# figure out what color to use
	COLOR=$(hostname|md5sum|perl -ne 'if (/^([a-f\d]{8})/) {
              my $num = unpack(q(L),pack(q(H*),$1));
              my @col = map {(qq(0;${_}m),qq(1;${_}m))} 31..37;
              my $col = $col[$num % @col];
              if ($col eq q(0;36m)) {
                 $col = q(1;30m);
              }
              print $col;
              }')
	PS1="${debian_chroot:+($debian_chroot)}\[\033[$COLOR\]\h \[\033[0;36m\]\t \w$\[\033[0m\] "
	unset COLOR
    else
	PS1="${debian_chroot:+($debian_chroot)}\[\033[0;36m\]\h \t \w$\[\033[0m\] "
    fi;
    # \[\033[1;36m\][\w]\n\[\033[0;36m\][\h|\t]$ \[\033[0m\]

    # make sure the term we're using exists in the termcap
    if [[ "$TERM" =~ "rxvt-" ]] && which tput >/dev/null 2>&1; then
        if [[ -z "$(tput longname 2>/dev/null)" ]]; then
            TERM="xterm-${TERM##rxvt-}"
            export TERM
        fi;
    fi;
    if [[ "$TERM" =~ "stterm-" ]] && which tput >/dev/null 2>&1; then
        if [[ -z "$(tput longname 2>/dev/null)" ]]; then
            TERM="xterm-${TERM##stterm-}"
            export TERM
        fi;
    fi;
    # If this is an xterm set the title to user@host:dir
    case $TERM in
	screen|xterm*|stterm*)
            PROMPT_COMMAND='echo -ne "\033]0;${USER}@${HOSTNAME}: ${PWD}\007"'
            ;;
	rxvt*)
            PROMPT_COMMAND='echo -ne "\033]2;${USER}@${HOSTNAME}: ${PWD}\007"'
            ;;

	*)
            ;;
    esac

    # enable programmable completion features (you don't need to enable
    # this, if it's already enabled in /etc/bash.bashrc).
    if [ -f /etc/bash_completion ]; then
      . /etc/bash_completion;
    fi;

    # set up ssh-agent
    if which ssh-agent >/dev/null && [ -d ~/.ssh ] &&  \
	[ -z "$SSH_CONNECTION" ] &&  [ -z "$SSH_CLIENT" ]; then 
#	for 
	if [ -e ~/.ssh/ssh_agent_info ] && \
	    [ -n "$(awk -F '[=;]' '/^SSH_AGENT_PID/{print $2}' ~/.ssh/ssh_agent_info)" ] && \
	    kill -0 "$(awk -F '[=;]' '/^SSH_AGENT_PID/{print $2}' ~/.ssh/ssh_agent_info)" >/dev/null 2>&1; then
	    . ~/.ssh/ssh_agent_info > /dev/null
	elif [ -e ~/.ssh/ssh_agent_info_$(hostname) ] && \
	    kill -0 "$(awk -F '[=;]' '/^SSH_AGENT_PID/{print $2}' ~/.ssh/ssh_agent_info_$(hostname))" >/dev/null 2>&1; then
	    . ~/.ssh/ssh_agent_info_$(hostname) > /dev/null
	else
	    ssh-agent -s > ~/.ssh/ssh_agent_info_$(hostname)
	    . ~/.ssh/ssh_agent_info_$(hostname) > /dev/null
	fi;
    fi;
    
    alias aumix2='aumix -d /dev/mixer1 -I'
    if locale -a |grep -qi en_US.utf8; then
	LANG="en_US.UTF-8"
    else
	LANG=C
    fi;
    export LANG
    
    # If we're running on liszt, include /var/list/.bin in the path
    if [ "$(hostname)" == "liszt" ] && [ -d /var/list/.bin ]; then
	PATH="/var/list/.bin:$PATH"
    fi;
    export PATH="$(getent passwd $(id -u)|awk -F: '{print $6}')/bin:$PATH"
    
    if [ ! -e ~/tmp ]; then
	mkdir ~/tmp;
    fi;
    export TMPDIR="$(echo ~/tmp)";

	cd ()
	{
	    # use pushd instead of cd
	    if [ -z "$1" ] || [ "$1" == "-" ]; then
		builtin cd "$@"; # && [ -r .todo ] && devtodo ${TODO_OPTIONS};
	    else
		builtin pushd "$@" >/dev/null; # && [ -r .todo ] && devtodo ${TODO_OPTIONS};
	    fi;
	}

fi
